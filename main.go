package main

import (
	"fmt"
	"math/rand"
	"time"
)

// Item represents an item that can be used or consumed by the player
type Item struct {
	name        string
	value       int
	description string
	use         func(*Player)
}

// Quest represents a task that the player must complete
type Quest struct {
	name             string
	description      string
	itemRewards      []Item
	experienceReward int
	completed        bool
}

// Player represents the player character
type Player struct {
	name             string
	level            int
	health           int
	maxHealth        int
	pos              position
	experience       int
	experienceNeeded int
	inventory        []Item
	quests           []Quest
	activeQuests     []int
	itemsUsed        map[string]int
}

type position struct {
	x int
	y int
}

// Enemy represents a hostile creature that the player can encounter
type Enemy struct {
	name   string
	health int
	damage int
	pos    position
}

func (p *Player) MoveNorth() {
	p.pos.y--
	fmt.Println("You move north.")
}

func (p *Player) MoveSouth() {
	p.pos.y++
	fmt.Println("You move south.")
}

func (p *Player) MoveEast() {
	p.pos.x++
	fmt.Println("You move east.")
}

func (p *Player) MoveWest() {
	p.pos.x--
	fmt.Println("You move west.")
}

func (p *Player) Attack(e *Enemy) {
	damage := rand.Intn(10) + 1
	e.health -= damage
}

func (p *Player) AddExperience(amount int) {
	p.experience += amount
	if p.experience >= p.experienceNeeded {
		p.LevelUp()
	}
}

func (p *Player) CompleteQuest(index int) {
	quest := p.quests[index]
	if !quest.completed {
		quest.completed = true
		p.experience += quest.experienceReward
		fmt.Printf("You completed the quest '%s' and received %d experience!\n", quest.name, quest.experienceReward)
		for _, item := range quest.itemRewards {
			p.AddItem(item)
		}
	}
}

func (p *Player) LevelUp() {
	p.level++
	p.experienceNeeded = p.level * 100
	p.maxHealth += 10
	p.health = p.maxHealth
	fmt.Printf("Congratulations, you have reached level %d!\n", p.level)
}

func (p *Player) UseItem(index int) {
	item := p.inventory[index]
	item.use(p)
	p.itemsUsed[item.name]++
	if p.itemsUsed[item.name] >= item.value {
		p.RemoveItem(index)
		delete(p.itemsUsed, item.name)
	}
}

func (p *Player) AddItem(item Item) {
	p.inventory = append(p.inventory, item)
}

func (p *Player) RemoveItem(index int) {
	p.inventory = append(p.inventory[:index], p.inventory[index+1:]...)
}

func (p *Player) HasActiveQuest(questIndex int) bool {
	for _, index := range p.activeQuests {
		if index == questIndex {
			return true
		}
	}
	return false
}

func parseInt(input string) int {
	var n int
	_, err := fmt.Sscanf(input, "%d", &n)
	if err != nil {
		return 0
	}
	return n
}

func main() {
	rand.Seed(time.Now().UnixNano())

	player := Player{
		name:             "Player",
		level:            1,
		health:           50,
		maxHealth:        50,
		pos:              position{0, 0},
		experience:       0,
		experienceNeeded: 100,
		inventory:        []Item{},
		quests:           []Quest{},
		activeQuests:     []int{},
		itemsUsed:        make(map[string]int),
	}

	goblin := Enemy{
		name:   "Goblin",
		health: 20,
		damage: 5,
		pos:    position{1, 1},
	}

	// Generate some items and quests
	items := []Item{
		{
			name:        "Health Potion",
			value:       3,
			description: "Restores 20 health.",
			use: func(p *Player) {
				p.health += 20
				if p.health > p.maxHealth {
					p.health = p.maxHealth
				}
			},
		},
		{
			name:        "Poison",
			value:       1,
			description: "Deals 10 damage over 5 turns.",
			use: func(p *Player) {
				for i := 0; i < 5; i++ {
					p.health -= 2
					if p.health <= 0 {
						break
					}
					time.Sleep(1 * time.Second)
				}
			},
		},
	}

	quests := []Quest{
		{
			name:        "Goblin Hunter",
			description: "Kill 3 goblins.",
			itemRewards: []Item{
				items[0],
			},
			experienceReward: 50,
			completed:        false,
		},
	}

	// Add some initial items and quests to the player
	player.AddItem(items[0])
	player.AddItem(items[1])
	player.quests = append(player.quests, quests[0])

	fmt.Printf("Welcome to the RPG game, %s!\n", player.name)
	fmt.Println("Type 'help' for a list of commands.")

	for {
		fmt.Printf("You are at position (%d, %d).\n", player.pos.x, player.pos.y)

		// Check for quests to add
		for i, quest := range quests {
			if !player.HasActiveQuest(i) && !quest.completed {
				fmt.Printf("New quest available: %s - %s\n", quest.name, quest.description)
				player.quests = append(player.quests, quest)
				player.activeQuests = append(player.activeQuests, i)
			}
		}

		// Check for quests to complete
		for _, i := range player.activeQuests {
			quest := player.quests[i]
			if !quest.completed {
				if quest.name == "Goblin Hunter" {
					if player.itemsUsed["Goblin Ear"] >= 3 {
						player.CompleteQuest(i)
						break
					}
				}
			}
		}

		// Check for combat with goblin
		if player.pos == goblin.pos {
			fmt.Printf("You encounter a %s!\n", goblin.name)
			for {
				player.Attack(&goblin)
				if goblin.health <= 0 {
					fmt.Printf("You defeated the %s!\n", goblin.name)
					player.AddExperience(25)
					player.AddItem(Item{
						name:        "Goblin Ear",
						description: "A gruesome trophy from a slain goblin.",
					})
					break
				}
				goblin.Attack(&player)
				if player.health <= 0 {
					fmt.Println("You died!")
					return
				}
				fmt.Printf("You have %d health.\n", player.health)
				fmt.Printf("The %s has %d health.\n", goblin.name, goblin.health)
				fmt.Println("Press enter to continue.")
				fmt.Scanln()
			}
		}

		// Get user input
		var input string
		fmt.Print("> ")
		fmt.Scanln(&input)

		if input == "north" {
			player.MoveNorth()
		} else if input == "south" {
			player.MoveSouth()
		} else if input == "east" {
			player.MoveEast()
		} else if input == "west" {
			player.MoveWest()
		} else if input == "inventory" {
			if len(player.inventory) == 0 {
				fmt.Println("Your inventory is empty.")
			} else {
				fmt.Println("Your inventory:")
				for i, item := range player.inventory {
					fmt.Printf("%d: %s - %s\n", i+1, item.name, item.description)
				}
			}
		} else if input == "quests" {
			if len(player.activeQuests) == 0 {
				fmt.Println("You have no active quests.")
			} else {
				fmt.Println("Your active quests:")
				for _, i := range player.activeQuests {
					quest := player.quests[i]
					if !quest.completed {
						fmt.Printf("%s - %s\n", quest.name, quest.description)
					}
				}
			}
		} else if input == "complete" {
			if len(player.activeQuests) == 0 {
				fmt.Println("You have no active quests.")
			} else {
				fmt.Println("Which quest do you want to complete?")
				for i, index := range player.activeQuests {
					quest := player.quests[index]
					if !quest.completed {
						fmt.Printf("%d: %s - %s\n", i+1, quest.name, quest.description)
					}
				}
				var input string
				fmt.Print("> ")
				fmt.Scanln(&input)
				index := parseInt(input) - 1
				if index >= 0 && index < len(player.activeQuests) {
					questIndex := player.activeQuests[index]
					player.CompleteQuest(questIndex)
					player.activeQuests = append(player.activeQuests[:index], player.activeQuests[index+1:]...)
				} else {
					fmt.Println("Invalid quest number.")
				}
			}
		} else if input == "use" {
			if len(player.inventory) == 0 {
				fmt.Println("Your inventory is empty.")
			} else {
				fmt.Println("Which item do you want to use?")
				for i, item := range player.inventory {
					fmt.Printf("%d: %s - %s\n", i+1, item.name, item.description)
				}
				var input string
				fmt.Print("> ")
				fmt.Scanln(&input)
				index := parseInt(input) - 1
				if index >= 0 && index < len(player.inventory) {
					player.UseItem(index)
				} else {
					fmt.Println("Invalid item number.")
				}
			}
		} else if input == "help" {
			fmt.Println("Commands:")
			fmt.Println("north - move north")
			fmt.Println("south - move south")
			fmt.Println("east - move east")
			fmt.Println("west - move west")
			fmt.Println("inventory - show inventory")
			fmt.Println("quests - show active quests")
			fmt.Println("complete - complete a quest")
			fmt.Println("use - use an item")
			fmt.Println("help - show help")
		} else {
			fmt.Println("Invalid command. Type 'help' for a list of commands.")
		}
	}
}

func (e *Enemy) Attack(p *Player) {
	p.health -= e.damage
	fmt.Printf("The %s attacks you for %d damage!\n", e.name, e.damage)
}
